# macOS: Open Anyway!

Open files anyway by bypassing Gatekeeper!

## The Magic
It appears that GateKeeper stores information regarding the files in the files' attributes.

By removing the attributes as well as 're-registering' the file in Finder, GateKeeper will not see the file being modified or downloaded elsewhere. This method was working, at least even in macOS Ventura.  

## Installation and Usage
### Installation
1. Go to the Releases section **of this page, not any of any fork, or any page that has similar text as this** and download the latest version.
2. Extract the archive. 
3. Open the resulting file with Automator Installer (usually the default). Follow the prompts to install it. 

### Usage
4. Open the context menu of the file **that you seriously want to bypass GateKeeper on. Never use this extension if you downloaded the app from a website that you do not usually browse on or is not known, as these may most likely contain malware.**
5. Hover over Quick Actions and click "open anyway."  
6. Follow the on-screen prompts. 

## Contribute
If you find anything that this software needs to improve on, feel free to leave feedback via the issue tracker! 

If you want to modify this code, feel free kindly create a fork. Once done, kindly create a pull request — do not create your own releases from your fork or any other forks.  



